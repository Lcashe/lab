#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// функція обчислювання квадрату матриці
void square_matrix(int arr_conv[], int arr_res[], int M) {
    for (int i = 0; i < M; i++)
        for (int j = 0; j < M; j++)
            arr_res[i * M + j] = 0;

    // застосування циклів для проходження за масивом 
    for (int i = 0; i < M; i++)
        for (int j = 0; j < M; j++)
            for (int k = 0; k < M; k++)
                // обчислювання результату за допомогою присвоювання помноження
                // масива arr_conv та допоміжного елементу k 
                arr_res[i * M + j] += arr_conv[i * M + k] * arr_conv[k * M + j];
}

// функція визначення чи є число простим
int is_digit_prime(int n) {
    // результат дорівнює 1 за замовчуванням, до перевірки циклом
    short res = 1;

    // цикл перевірки числа на простоту 
    for (int i = 2; i < n / 2; i++)
        // якщо число ділиться на 2 без остачі, тоді число не є простим
        if (n % i == 0) {
            res = 0;
            break;
        }

    // якщо число меньше або дорівнює 1, то воно не є простим
    if (n <= 1)
        res = 0;

    // вертає змінну res типу int (як і тип функції)
    return res;
}


int main(int argc, char** argv) {
    // встановлює поточний час. Це робиться задля того,
    // щоб функція rand() генерувала випадкові числа,
    // через те що час змінюється
    srand((unsigned int)time(NULL));

    // якщо argc дорівнює нулю, це означає, що в консолі вводиться
    // лише адрес програми, без яких небудь значень змінних, маючи наступний вид:
    // gdb --args ./lab07
    if (argc == 1)
        return 0;

    /* якщо перший елемент argc дорівнює нулю,
       тоді виконується завдання із масивами
    */
    if (atoi(argv[1]) == 0) {
        int M = atoi(argv[2]);

        // визначення масивів
        int arr[M][M];
        int arr_conv[M * M];
        int arr_res[M * M];

        // заповнення масива за допомогою випадкових чисел
        for (int i = 0; i < M; i++)
            for (int j = 0; j < M; j++)
                arr[i][j] = rand() % 10 + 1;

        // преобразование двумерного массива в одномерный
        for (int i = 0; i < M; i++)
            for (int j = 0; j < M; j++)
                arr_conv[i * M + j] = arr[i][j];

        // визов функції
        square_matrix(arr_conv, arr_res, M);

        // перетворення двовимірного масива на одномірний
        int printed_array[M][M];
        for (int i = 0; i < M; i++)
            for (int j = 0; j < M; j++)
                printed_array[i][j] = arr_res[i * M + j];

        /* якщо аргументів 3 або більше, тоді
        *  умова задана правильно */
        if (argc >= 3) {
            // змінна для визначення розміру матриці, із командних аргументів
            int M = atoi(argv[2]);
            
            // кількість argc-3 повинен дорівнювати розміру матриці у квадраті
            if (argc - 3 == M*M) {
                for (int i = 0; i < M; i++)
                    for (int j = 0; j < M; j++)
                        arr_conv[i * M + j] = atoi(argv[i * M + j + 3]);
                // виклик функції
                square_matrix(arr_conv, arr_res, M);

                // перетворення двовимірного масива на одномірний
                int arg_printed_arr[M][M];
                for (int i = 0; i < M; i++)
                    for (int j = 0; j < M; j++)
                        arg_printed_arr[i][j] = arr_res[i * M + j];
            }
        }
    }

    /*
        якщо другий параметр командної строки дорівнює 1, 
        тоді виконуємо програму визначення простоти числа 
    */
    else if (atoi(argv[1]) == 1) {
        if (argc == 3) {
            int n = atoi(argv[2]);

            // викликаємо функцію із застосуванням третього параметра командної строки
            int result = is_digit_prime(n);
        }
    }

    return 0;
}

/*

Usage of args, argv:

for arrays:
./lab07 0 3 1 2 3 4 5 6 7 8 9

for cicle:
./lab07 1 5

*/