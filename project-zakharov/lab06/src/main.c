#include <stdio.h>

int main() {
    // масив повинен бути константним
    // N - число строк та стовпчик
    const int N = 3;

    // ініціалізація масива arr із строк та стовпчиків розміру N * N
    int arr[N][N];
    // ініціалізація масива arr із строк та стовпчиків розміру N * N
    int arr_res[N][N];

    // цикл заповнення масиву числами 
    // змінна i для масиву arr -> arr[i][N];
    for (int i = 0; i < N; i++) {
        // змінна j для масиву arr -> arr[j][j];
        for (int j = 0; j < N; j++)
            arr[i][j] = i * j + 1;
    }

    // цикл дорівнювання масиву arr_res до нулю
    // змінна i для масиву arr -> arr_res[i][N];
    for (int i = 0; i < N; i++) {
        // змінна j для масиву arr -> arr_res[j][j];
        for (int k = 0; k < N; k++)
            arr_res[k][i] = 0;
    }

    // цикл рахування результату. Помноження двувимірного масиву самого на себе
    // за правилом помноження матриць. (стовбчик[j] * строку[i])
    for (int k = 0; k < N; k++) {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) 
                arr_res[k][i] += arr[k][j] * arr[j][i];
        }
    }

    return 0;
}