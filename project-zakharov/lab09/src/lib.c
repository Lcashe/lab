#include "lib.h"

void square_matrix(int arr[], int arr_res[], int M) {
    for (int i = 0; i < M; i++)
        for (int j = 0; j < M; j++)
            arr_res[i * M + j] = 0;

    for (int i = 0; i < M; i++)
        for (int j = 0; j < M; j++)
            for (int k = 0; k < M; k++)
                arr_res[i * M + j] += arr[i * M + k] * arr[k * M + j];
}

int is_digit_prime(int n) {
    short res = 1;

    for(int i = 2; i < n; i++) {
        if (n % i == 0) {
            res = 0;
            break;
        }
    }

    if (n <= 1)
        res = 0;

    return res;
}