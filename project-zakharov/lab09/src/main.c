#include "lib.h"

int main(int argc, char** argv) {
    if (argc == 1) 
        return 0;

    if (atoi(argv[1]) == 0) {
        int M = atoi(argv[2]);

        int arr[4 * 4];
        int arr_res[4 * 4];

        for (int i = 0; i < M; i++)
            arr[i] = rand() % 10;

        square_matrix(arr, arr_res, M);

        if (argc - 3 == M*M) {
            for (int i = 0; i < M; i++)
                for (int j = 0; j < M; j++)
                    arr[i * M + j] = atoi(argv[i * M + j + 3]);

            square_matrix(arr, arr_res, M);
        }
    }

    else if (atoi(argv[1]) == 1) {
        if (argc == 3) {
            int n = atoi(argv[2]);

            int result = is_digit_prime(n);
        }
    }
} 