#ifndef LIB_H
#define LIB_H

#include <stdio.h>
#include <stdlib.h>

void square_matrix(int arr[], int arr_res[], int M);
int is_digit_prime(int n);

#endif 