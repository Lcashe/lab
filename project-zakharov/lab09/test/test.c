#include "../src/lib.h"
#include <check.h>

START_TEST (prime_digit_test) {
#define ARRAY_SIZE 3
    int initial_data[] = {1, 7, 10};
    int expected_values[] = {0, 1, 0};

    for (int i = 0; i < ARRAY_SIZE; i++) {
        int actual_result = is_digit_prime(initial_data[i]);
        ck_assert_int_eq(expected_values[i], actual_result);
    }
}
END_TEST

START_TEST(array_test) {
#define ARRAY_SIZE 3

    int initial_array[4*4] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    int result_array[4*4];
    int expected_array[] = {30, 36, 42, 66, 81, 96, 102, 126, 150};

    square_matrix(initial_array, result_array, ARRAY_SIZE);

    for (int i = 0; i < ARRAY_SIZE * ARRAY_SIZE; i++)
        ck_assert_int_eq(expected_array[i], result_array[i]);
}
END_TEST

int main(void) {
    Suite *s = suite_create("Programming");
    TCase *tc_core = tcase_create("lab09");

    tcase_add_test(tc_core, prime_digit_test);
    tcase_add_test(tc_core, array_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}