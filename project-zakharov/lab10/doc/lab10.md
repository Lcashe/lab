# Захаров Владислав Олегович / КНІТ-321а

## Лабораторна робота №10. Вступ до показчиків 

## Завдання: Дано масив масивів з N * N цілих чисел. Елементи головної діагоналі записати в одновимірний масив, отриманий масив упорядкувати за зростанням.

### Основна частина: 

#### Перелік вхідних даних:

- ``int** arr`` - двовимірний динамічний масив

- ``int* arr_res`` - одновимірний динамічний масив

#### Перелік функцій:

- ``void computating_array(int** arr, int* arr_res)`` - функція обчислювання результату завдання

#### Структура проекту лабораторної роботи:
```
├── README.md
├── dist
│   └── html
├── Doxyfile
├── Makefile
├── src
│   ├── lib.c
│   ├── lib.h
│   └── main.c
├── doc
│   └──lab10.md
└── test
    └── test.c
```