#include "lib.h"

void computating_array(int** arr, int* arr_res) {
 
    for (int i = 0; i < ARRAY_SIZE; i++)
        for (int j = 0; j < ARRAY_SIZE; j++)
            if(i == j) 
                arr_res[i] = arr[i][j];

    for (int i = 0; i < ARRAY_SIZE - 1; i++) {
        for (int j = 0; j < ARRAY_SIZE - i - 1; j++) {
            if (arr_res[i] > arr_res[j + 1]) {
                int temp = arr_res[j];
                arr_res[j] = arr_res[j + 1];
                arr_res[j + 1] = temp;
            }
        }
    }

}