#include "lib.h"

int main() {
    int** arr = (int**)malloc(ARRAY_SIZE * sizeof(int*));
    for (int i = 0; i < ARRAY_SIZE; i++)
        *(arr + i) = (int*)malloc(ARRAY_SIZE * sizeof(int));

    int* arr_res = (int*)malloc(ARRAY_SIZE * sizeof(int));

    for (int i = 0; i < ARRAY_SIZE; i++)
        for (int j = 0; j < ARRAY_SIZE; j++)
            arr[i][j] = rand() % 10;

    computating_array(arr, arr_res);

    for (int i = 0; i < ARRAY_SIZE; i++)
        free(*(arr+i));
    free(arr);

    free(arr_res);

    return 0;
}