#include "../src/lib.h"
#include <check.h>

START_TEST(array_test) {
#define ARRAY_SIZE 3

    int values[] = {1, 3, 5, 2, 6, 4, 8, 7, 9};

    int **initial_array = (int**)malloc(ARRAY_SIZE * sizeof(int*));
    for (int i = 0; i < ARRAY_SIZE; i++)
        *(initial_array + i) = (int*)malloc(ARRAY_SIZE * sizeof(int*));

    for (int i = 0; i < ARRAY_SIZE; i++)
        for (int j = 0; j < ARRAY_SIZE; j++)
            initial_array[i][j] = values[i * ARRAY_SIZE + j];

    int expected_array[ARRAY_SIZE] = {1, 6, 9};

    int *result_array = (int*)malloc(ARRAY_SIZE * sizeof(int));

    computating_array(initial_array, result_array);

    for (int i = 0; i < ARRAY_SIZE; i++)
        result_array[i] = result_array[i];

    for (int i = 0; i < ARRAY_SIZE; i++)
        ck_assert_int_eq(expected_array[i], result_array[i]);
}
END_TEST

int main(void) {
    Suite *s = suite_create("Programming");
    TCase *tc_core = tcase_create("lab09");

    tcase_add_test(tc_core, array_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}